Project attempting to clarify Erlang's NIFs with examples.
1. factorial_nif 
	Factorial implemented as NIF
	Details inside.

2. template_nif
	Example from: http://www.erlang.org/doc/tutorial/nif.html
	With a little modification and comments inside the code.
	Details inside.
Sources:
http://www.erlang.org/doc/tutorial/example.html
http://www.erlang.org/doc/tutorial/c_portdriver.html
http://www.erlang.org/doc/tutorial/nif.html#id64877
http://www.erlang.org/doc/man/erl_nif.html
http://www.erlang.org/doc/man/erlang.html#load_nif-2
http://erlang.org/pipermail/erlang-questions/2013-May/073627.html
https://code.google.com/p/erlang-quiz/wiki/InstallingErlangLinux
http://www.chicagoerlang.com/steve-vinoski.html
https://www.youtube.com/watch?v=57AkoJfojK8