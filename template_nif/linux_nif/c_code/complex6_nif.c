#include "erl_nif.h" //must be included

//functions from external .c file
extern int foo(int x);
extern int bar(int y);

/*
	argv - array with arguments passed to a NIF
	argc - lenght of the array
	env - handle needed to be passed on to most API functoins
		contains information about calling Erlang process
*/
static ERL_NIF_TERM foo_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int x, ret;
    if (!enif_get_int(env, argv[0], &x)) {
	return enif_make_badarg(env);
    }
    ret = foo(x);
    return enif_make_int(env, ret);
}

static ERL_NIF_TERM bar_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int y, ret;
    if (!enif_get_int(env, argv[0], &y)) {
	return enif_make_badarg(env);
    }
    ret = bar(y);
    return enif_make_int(env, ret);
}

static ErlNifFunc nif_funcs[] = {
	/*
		"name_to_be_called_from_erlang_module", 
		number_of_arguments,
		name_of_implemented_above_function
	*/
    {"foo", 1, foo_nif},
    {"bar", 1, bar_nif}
};
/*
	ERL_NIF_INIT arguments:
	-name of a module
	-array containing information about loaded nif functions
	-other: pointers to callback functions that can be used to initialize the
	library
*/
ERL_NIF_INIT(complex6, nif_funcs, NULL, NULL, NULL, NULL) 