#include<stdio.h>
#include<stdlib.h>

///21-10-2013

/*
	Programm that can effectively count factorials
	of numbers which are as long as an array can be
*/

int* factorial(int n)
{
    const int range=20000;	//max. number of digits in a result
    int res_size = range;   //size of result array + first field with lenght of result

    int tab[range];
    int i,j,p;

    for(i=0; i<range; i++){ ///(1,0,0,0,...)
        tab[i]=0;
    }
    tab[0]=1;

    for(i=1; i<=n; i++){                ///multiplying number by another element
        p=0;                            ///
        for(j=0; j<range; j++){
            tab[j]=tab[j]*i+p;
            p=tab[j]/10;
            tab[j]=tab[j]%10;
        }
        if(p>0){break;}   ///to avoid going outside the array
    }

    for(i=range-1; i>=0; i--){
        if(tab[i]!=0){
            res_size=i+1;
            break;
        }
    }
    /*
        result array consist of 1 + (number of digits in result) elements
        number of digits in result can be found in the element with index 0
    */
    int* result = (int*)calloc(res_size+1,sizeof(int));
    result[0] = res_size;
    for(int i=1; i<res_size+1; i++)
        result[i] = tab[res_size-1-(i-1)];
    return result;
}



/*int main(){
    int *a = factorial(5500);
    int size_a = a[0];
    printf("size %d\n",size_a);
    for(int i=1; i<size_a+1; i++){
        printf("%d",a[i]);
    }
    return 0;
}*/
