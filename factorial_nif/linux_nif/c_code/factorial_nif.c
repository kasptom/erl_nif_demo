#include "erl_nif.h" //must be included
#include<stdio.h>
//functions from external .c file
extern int* factorial(int n);

/*
	argv - array with arguments passed to a NIF
	argc - lenght of the array
	env - handle needed to be passed on to most API functoins
		contains information about calling Erlang process
*/
static ERL_NIF_TERM factorial_nif(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    int n;			//number which factorial we want to calculate
    int* res;			//array from result from factorial.c -> factorial(n)
    if (!enif_get_int(env, argv[0], &n)) {	//from args we try to read an argument
	return enif_make_badarg(env);
    }
    res = factorial(n);		//calling native c function
    unsigned res_size = res[0]; //first element is the number of digits in the result
	printf("res size %d\n",res_size);
    ERL_NIF_TERM tail = enif_make_list(env,0);	//creating an empty list
    ERL_NIF_TERM head;
    for(int i=(res_size-1)+1; i>=1; i--){
    	head = enif_make_int(env,res[i]+48);
	tail = enif_make_list_cell(env,head,tail);
    }
	return tail;
    /* //alternative way of doing it using Erlang's array
	ERL_NIF_TERM result[res_size];	
	for(int i = 1; i<res_size+1; i++)
		result[i-1] = enif_make_int(env,res[i]+48); //to ASCII code
	return enif_make_list_from_array(env,result,res_size);
    */
}

static ErlNifFunc nif_funcs[] = {
	/*
		"name_to_be_called_from_erlang_module", 
		number_of_arguments,
		name_of_implemented_above_function
	*/
    {"factorial", 1, factorial_nif},
};
/*
	ERL_NIF_INIT arguments:
	-name of a module
	-array containing information about loaded nif functions
	-other: pointers to callback functions that can be used to initialize the
	library
*/
ERL_NIF_INIT(factorial, nif_funcs, NULL, NULL, NULL, NULL) 
