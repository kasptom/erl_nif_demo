-module(factorial).

%% API
-export([factorial/1]).
%Here we specify which functions  fill be launched immediately after module loading 
-on_load(init/0). 
%At start using erlang:load_nif/2 (Path of .so file compiled from C, LoadInfo)
%we load functions that were compiled from C and packed into .so file
init() ->
  ok = erlang:load_nif("../c_code/factorial_nif", 0).

%If NIF's don't load, below functions will be called.
factorial(_X) ->
  exit(nif_library_not_loaded).

