How to launch
I. On Linux:
	1. Go to linux_nif/c_code
	2. Compile files in above directory with command:
	gcc -o factorial_nif.so -fpic -shared factorial.c factorial_nif.c -std=c99
	3. Go to linux_nif/erl_code.
	4. Launch erlang console: erl
	5. Compile file factorial.erl:
		1> c(factorial).
		{ok, factorial}
	5. In orther to call function type module name and colon
	name of called finction implemented in c:
		2>factorial:factorial(5).
		120
		
II. For Windows
Note: Attempt to use gcc on Windows ends up with fail
	probable reason incorect .so file is created
       attemt of changing .so extension on .dll in gcc -o ... command gives no result
	
	In orther to compile code on Windows we c.exe compilator is needed
	It can be installed with Microsoft Visual Studio